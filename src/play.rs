use crate::components::{Component, ComponentSource};
use crate::consts::MidiMessage;
use crate::read_midi::read_midi;
use rodio::Source;
use std::sync::mpsc;

pub fn play<T: Component + Send + 'static>(component: T, midi_tx: mpsc::Sender<MidiMessage>) {
    let (_stream, stream_handle) = rodio::OutputStream::try_default().unwrap();

    let source = ComponentSource::new(component);

    stream_handle.play_raw(source.convert_samples()).unwrap();

    match read_midi(midi_tx) {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err),
    }
}
