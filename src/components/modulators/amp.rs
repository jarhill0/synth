use crate::components::{AdsrEnvelope, Component, Envelope};
use crate::consts::*;

pub struct AmpModulator<T: Component + Send, U: AmpSignal> {
    component: T,
    amp_signal: U,
    channels: u16,
    amount: f32,
    current_channel: u16,
    cached_amp: f32,
}

impl<T: Component + Send, U: AmpSignal> AmpModulator<T, U> {
    pub fn new(component: T, amp_signal: U, amount: f32) -> AmpModulator<T, U> {
        assert!(0.0 <= amount && amount <= 1.0);
        let channels = component.channels();
        AmpModulator {
            component,
            amp_signal,
            channels,
            amount,
            current_channel: 0,
            cached_amp: 0.0,
        }
    }

    fn get_amp(&mut self) -> f32 {
        if self.current_channel == 0 {
            self.cached_amp = self.amp_signal.step();
        }
        self.current_channel = (self.current_channel + 1) % self.channels;
        self.cached_amp
    }
}

impl<T: Component + Send, U: AmpSignal> Component for AmpModulator<T, U> {
    fn get_sample(&mut self) -> Sample {
        let component_sample = self.component.get_sample();
        let amp = self.get_amp();
        (self.amount * amp + (1.0 - self.amount)) * component_sample
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn set_freq(&mut self, freq: f32) {
        self.component.set_freq(freq);
    }

    fn set_amp(&mut self, amp: f32) {
        self.component.set_amp(amp);
    }

    fn attack(&mut self) {
        self.amp_signal.attack();
        self.component.attack();
    }
    fn release(&mut self) -> u32 {
        self.amp_signal.release()
    }
}

pub trait AmpSignal {
    fn step(&mut self) -> f32;
    fn attack(&mut self);
    fn release(&mut self) -> u32;
}

impl AmpSignal for AdsrEnvelope {
    fn step(&mut self) -> f32 {
        Envelope::step(self)
    }
    fn attack(&mut self) {
        Envelope::attack(self);
    }
    fn release(&mut self) -> u32 {
        Envelope::release(self)
    }
}

impl<T: Component> AmpSignal for T {
    fn step(&mut self) -> f32 {
        (self.get_sample() + 1.0) / 2.0
    }
    fn attack(&mut self) {
        self.attack()
    }
    fn release(&mut self) -> u32 {
        0
    }
}
