pub trait Envelope {
    fn attack(&mut self);
    fn release(&mut self) -> u32;

    fn step(&mut self) -> f32; // should be in [0.0, 1.0]
}
