use crate::components::Envelope;
use crate::consts::SAMPLES_PER_SECOND;

pub struct AdsrEnvelope {
    attack: f32,  // seconds
    decay: f32,   // seconds
    sustain: f32, // amplitude in [0.0, 1.0]
    release: f32, // seconds

    step: f32,            // seconds per sample
    release_samples: u32, // # of samples from release trigger to end
    t: f32,               // seconds elapsed in current stage

    stage: AdsrStage,
}

impl AdsrEnvelope {
    pub fn new(attack: f32, decay: f32, sustain: f32, release: f32) -> AdsrEnvelope {
        AdsrEnvelope::new_with_options(attack, decay, sustain, release, None)
    }

    pub fn new_with_options(
        attack: f32,
        decay: f32,
        sustain: f32,
        release: f32,
        sample_rate: Option<u32>,
    ) -> AdsrEnvelope {
        let sample_rate = sample_rate.unwrap_or(SAMPLES_PER_SECOND);

        assert!(attack >= 0.0);
        assert!(decay >= 0.0);
        assert!(0.0 <= sustain && sustain <= 1.0);
        assert!(release >= 0.0);

        AdsrEnvelope {
            attack,
            decay,
            sustain,
            release,
            release_samples: (sample_rate as f32 * release) as u32,
            step: 1.0 / sample_rate as f32,
            t: 0.0,
            stage: AdsrStage::Inactive,
        }
    }

    fn level(&self) -> f32 {
        match self.stage {
            Inactive => 0.0,
            Attack => self.attack_level(),
            Decay => self.decay_level(),
            Sustain => self.sustain,
            Release(from) => self.release_level(from),
        }
    }

    fn advance(&mut self, level: f32) {
        self.t += self.step;
        match self.stage {
            Inactive => (),
            Attack => {
                if level >= 1.0 {
                    self.switch_stage(Decay);
                }
            }
            Decay => {
                if level <= self.sustain {
                    self.switch_stage(Sustain);
                }
            }
            Sustain => (),
            Release(_) => {
                if level <= 0.0 {
                    self.switch_stage(Inactive);
                }
            }
        }
    }

    fn switch_stage(&mut self, stage: AdsrStage) {
        self.stage = stage;
        self.t = 0.0;
    }

    fn attack_level(&self) -> f32 {
        let level = self.t / self.attack;
        min(level, 1.0)
    }

    fn decay_level(&self) -> f32 {
        let level = 1.0 - (1.0 - self.sustain) * self.t / self.decay;
        max(level, self.sustain)
    }

    fn release_level(&self, from: f32) -> f32 {
        let level = from - from * self.t / self.release;
        max(level, 0.0)
    }
}

impl Envelope for AdsrEnvelope {
    fn attack(&mut self) {
        self.switch_stage(Attack);
    }
    fn release(&mut self) -> u32 {
        self.switch_stage(Release(self.level()));
        self.release_samples
    }

    fn step(&mut self) -> f32 {
        let level = self.level();
        self.advance(level);
        level
    }
}

#[derive(Copy, Clone, Debug)]
enum AdsrStage {
    Inactive,
    Attack,
    Decay,
    Sustain,
    Release(f32), // starting level (in case released during attack or decay)
}
use AdsrStage::*;

fn max(a: f32, b: f32) -> f32 {
    if a.is_nan() && b.is_nan() {
        panic!("2 NaNs!");
    }
    if b.is_nan() || a >= b {
        a
    } else {
        b
    }
}

fn min(a: f32, b: f32) -> f32 {
    if a.is_nan() && b.is_nan() {
        panic!("2 NaNs!");
    }
    if b.is_nan() || a <= b {
        a
    } else {
        b
    }
}
