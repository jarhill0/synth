use crate::components::Component;
use crate::consts::*;
use rodio::Source;
use std::time::Duration;

pub struct ComponentSource<T: Component + Send> {
    component: T,
}

impl<T: Component + Send> ComponentSource<T> {
    pub fn new(component: T) -> ComponentSource<T> {
        ComponentSource { component }
    }
}

impl<T: Component + Send> Iterator for ComponentSource<T> {
    type Item = Sample;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.component.get_sample())
    }
}

impl<T: Component + Send> Source for ComponentSource<T> {
    fn channels(&self) -> u16 {
        self.component.channels()
    }

    fn sample_rate(&self) -> u32 {
        SAMPLES_PER_SECOND
    }

    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn total_duration(&self) -> Option<Duration> {
        None
    }
}
