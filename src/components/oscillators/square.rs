use crate::components::Oscillator;
use crate::consts::*;

#[derive(Copy, Clone)]
pub struct SquareOscillator {
    sample_rate: u32, // samples per second
    t: u32,           // sample count
    amp: f32,         // [0.0–1.0]
    base_amp: f32,    // unchanging. multiplied by `amp` to get total effective amplitude
    threshold: f32,   // amp where square switches sides

    step: f32,
}

impl SquareOscillator {
    pub fn new(freq: f32, base_amp: f32) -> SquareOscillator {
        SquareOscillator::new_with_options(freq, base_amp, None, None, None)
    }

    pub fn new_with_options(
        freq: f32,
        base_amp: f32,
        amp: Option<f32>,
        sample_rate: Option<u32>,
        threshold: Option<f32>,
    ) -> SquareOscillator {
        let amp = amp.unwrap_or(1.0);
        let threshold = threshold.unwrap_or(0.0);
        let sample_rate = sample_rate.unwrap_or(SAMPLES_PER_SECOND);

        SquareOscillator {
            sample_rate,
            amp,
            base_amp,
            threshold,
            t: 0,
            step: SquareOscillator::step(freq, sample_rate),
        }
    }

    fn step(freq: f32, sample_rate: u32) -> f32 {
        freq * 2. * PI / sample_rate as f32
    }
}

impl Oscillator for SquareOscillator {
    fn set_freq(&mut self, freq: f32) {
        self.step = SquareOscillator::step(freq, self.sample_rate);
        self.t = 0; // necessary? correct?
    }

    fn set_amp(&mut self, amp: f32) {
        self.amp = amp;
    }

    fn get_sample(&mut self) -> Sample {
        let value = (self.step * self.t as f32).sin();
        self.t += 1;
        if value > self.threshold {
            self.amp * self.base_amp
        } else {
            -self.amp * self.base_amp
        }
    }
}
