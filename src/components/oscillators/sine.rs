use crate::components::Oscillator;
use crate::consts::*;

#[derive(Copy, Clone)]
pub struct SineOscillator {
    sample_rate: u32, // samples per second
    t: u32,           // sample count
    amp: f32,         // [0.0–1.0]
    base_amp: f32,    // unchanging. multiplied by `amp` to get total effective amplitude

    step: f32,
}

impl SineOscillator {
    pub fn new(freq: f32, base_amp: f32) -> SineOscillator {
        SineOscillator::new_with_options(freq, base_amp, None, None)
    }

    pub fn new_with_options(
        freq: f32,
        base_amp: f32,
        amp: Option<f32>,
        sample_rate: Option<u32>,
    ) -> SineOscillator {
        let amp = amp.unwrap_or(1.0);
        let sample_rate = sample_rate.unwrap_or(SAMPLES_PER_SECOND);

        SineOscillator {
            sample_rate,
            base_amp,
            amp,
            t: 0,
            step: SineOscillator::step(freq, sample_rate),
        }
    }

    fn step(freq: f32, sample_rate: u32) -> f32 {
        freq * 2. * PI / sample_rate as f32
    }
}

impl Oscillator for SineOscillator {
    fn set_freq(&mut self, freq: f32) {
        self.step = SineOscillator::step(freq, self.sample_rate);
        self.t = 0; // necessary? correct?
    }

    fn set_amp(&mut self, amp: f32) {
        self.amp = amp;
    }

    fn get_sample(&mut self) -> Sample {
        let value = (self.step * self.t as f32).sin() * self.amp * self.base_amp;
        self.t += 1;
        value
    }
}
