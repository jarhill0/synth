use crate::components::Oscillator;
use crate::consts::*;
use std::cmp::min;

#[derive(Copy, Clone)]
pub struct WhiteNoiseOscillator {
    amp: f32,      // [0.0–1.0]
    base_amp: f32, // unchanging. multiplied by `amp` to get total effective amplitude
}

impl WhiteNoiseOscillator {
    pub fn new(base_amp: f32) -> WhiteNoiseOscillator {
        WhiteNoiseOscillator::new_with_options(base_amp, None)
    }

    pub fn new_with_options(base_amp: f32, amp: Option<f32>) -> WhiteNoiseOscillator {
        let amp = amp.unwrap_or(1.0);

        WhiteNoiseOscillator { base_amp, amp }
    }
}

impl Oscillator for WhiteNoiseOscillator {
    fn set_freq(&mut self, _freq: f32) {}

    fn set_amp(&mut self, amp: f32) {
        self.amp = amp;
    }

    fn get_sample(&mut self) -> Sample {
        let val = rand::random::<f32>() * 2.0 - 1.0;
        val * self.amp * self.base_amp
    }
}

const PINK_NOISE_LEVELS: usize = 11;

#[derive(Copy, Clone)]
pub struct PinkNoiseOscillator {
    amp: f32,                                     // [0.0–1.0]
    base_amp: f32, // unchanging. multiplied by `amp` to get total effective amplitude
    value: Sample, // the cached output value
    sub_samples: [Sample; PINK_NOISE_LEVELS + 1], // the value of each individual white noise sample
    level_count: f32, // the number of levels as a float for division
    t: u32,
}

impl PinkNoiseOscillator {
    pub fn new(base_amp: f32) -> PinkNoiseOscillator {
        PinkNoiseOscillator::new_with_options(base_amp, None)
    }

    pub fn new_with_options(base_amp: f32, amp: Option<f32>) -> PinkNoiseOscillator {
        let amp = amp.unwrap_or(1.0);
        let mut sub_samples = [0.0; PINK_NOISE_LEVELS + 1];
        for i in 0..sub_samples.len() {
            sub_samples[i] = PinkNoiseOscillator::random_amplitude();
        }
        let level_count = sub_samples.len() as f32;
        let value = sub_samples.iter().map(|v| v / level_count).sum();

        PinkNoiseOscillator {
            base_amp,
            amp,
            value,
            sub_samples,
            level_count,
            t: 0,
        }
    }

    fn random_amplitude() -> f32 {
        rand::random::<f32>() * 2.0 - 1.0
    }

    fn adjust_sample(&mut self, idx: usize) {
        // subtract prev val; add new one
        let old_val = self.sub_samples[idx];
        let new_val = PinkNoiseOscillator::random_amplitude();
        self.sub_samples[idx] = new_val;

        self.value += (new_val - old_val) / self.level_count;
    }

    fn advance(&mut self) {
        let idx = min(self.t.trailing_zeros() as usize, PINK_NOISE_LEVELS - 1);
        self.adjust_sample(idx);
        self.adjust_sample(PINK_NOISE_LEVELS); // update this sample every step

        self.t += 1;
    }
}

impl Oscillator for PinkNoiseOscillator {
    fn set_freq(&mut self, _freq: f32) {}

    fn set_amp(&mut self, amp: f32) {
        self.amp = amp;
    }

    fn get_sample(&mut self) -> Sample {
        if self.amp * self.base_amp != 0.0 {
            self.advance();
            self.value * self.amp * self.base_amp
        } else {
            0.0
        }
    }
}
