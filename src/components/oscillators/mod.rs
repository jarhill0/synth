mod noise;
mod oscillator;
mod sawtooth;
mod sine;
mod square;
mod triangle;

pub use noise::{PinkNoiseOscillator, WhiteNoiseOscillator};
pub use oscillator::Oscillator;
pub use sawtooth::SawtoothOscillator;
pub use sine::SineOscillator;
pub use square::SquareOscillator;
pub use triangle::TriangleOscillator;
