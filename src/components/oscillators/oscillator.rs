use crate::components::Component;
use crate::consts::Sample;

pub trait Oscillator: Copy {
    fn get_sample(&mut self) -> Sample;
    fn channels(&self) -> u16 {
        1
    }

    fn set_freq(&mut self, freq: f32);
    fn set_amp(&mut self, amp: f32);
}

impl<T: Oscillator> Component for T {
    fn get_sample(&mut self) -> Sample {
        self.get_sample()
    }
    fn channels(&self) -> u16 {
        1
    }

    fn set_freq(&mut self, freq: f32) {
        self.set_freq(freq);
    }
    fn set_amp(&mut self, amp: f32) {
        self.set_amp(amp);
    }
}
