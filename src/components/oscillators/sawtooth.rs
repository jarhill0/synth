use crate::components::Oscillator;
use crate::consts::*;

#[derive(Copy, Clone)]
pub struct SawtoothOscillator {
    sample_rate: u32, // samples per second
    t: u32,           // sample count
    amp: f32,         // [0.0–1.0]
    base_amp: f32,    // unchanging. multiplied by `amp` to get total effective amplitude

    step: f32,
}

impl SawtoothOscillator {
    pub fn new(freq: f32, base_amp: f32) -> SawtoothOscillator {
        SawtoothOscillator::new_with_options(freq, base_amp, None, None)
    }

    pub fn new_with_options(
        freq: f32,
        base_amp: f32,
        amp: Option<f32>,
        sample_rate: Option<u32>,
    ) -> SawtoothOscillator {
        let amp = amp.unwrap_or(1.0);
        let sample_rate = sample_rate.unwrap_or(SAMPLES_PER_SECOND);

        SawtoothOscillator {
            sample_rate,
            amp,
            base_amp,
            t: 0,
            step: SawtoothOscillator::step(freq, sample_rate),
        }
    }

    fn step(freq: f32, sample_rate: u32) -> f32 {
        2.0 * freq / sample_rate as f32
    }
}

impl Oscillator for SawtoothOscillator {
    fn set_freq(&mut self, freq: f32) {
        self.step = SawtoothOscillator::step(freq, self.sample_rate);
        self.t = 0; // necessary? correct?
    }

    fn set_amp(&mut self, amp: f32) {
        self.amp = amp;
    }

    fn get_sample(&mut self) -> Sample {
        let value = ((self.step * self.t as f32).rem_euclid(2.0) - 1.0) * self.amp * self.base_amp;
        self.t += 1;
        value
    }

    fn channels(&self) -> u16 {
        1
    }
}
