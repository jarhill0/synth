use crate::components::Component;
use crate::consts::*;

pub struct WaveAdder<T: Component + Send, U: Component + Send> {
    component_a: T,
    component_b: U,
    channel_count: u16,
}

impl<T: Component + Send, U: Component + Send> WaveAdder<T, U> {
    pub fn new(component_a: T, component_b: U) -> WaveAdder<T, U> {
        let channel_count =
            WaveAdder::assert_and_get_consistent_channel_count(&component_a, &component_b);
        WaveAdder {
            component_a,
            component_b,
            channel_count,
        }
    }

    fn assert_and_get_consistent_channel_count(component_a: &T, component_b: &U) -> u16 {
        assert!(component_a.channels() == component_b.channels());
        component_a.channels()
    }
}

impl<T: Component + Send, U: Component + Send> Component for WaveAdder<T, U> {
    fn get_sample(&mut self) -> Sample {
        (self.component_a.get_sample() + self.component_b.get_sample()) / 2.0
    }

    fn channels(&self) -> u16 {
        self.channel_count
    }

    fn set_freq(&mut self, freq: f32) {
        self.component_a.set_freq(freq);
        self.component_b.set_freq(freq);
    }

    fn set_amp(&mut self, amp: f32) {
        self.component_a.set_amp(amp);
        self.component_b.set_amp(amp);
    }
    fn attack(&mut self) {
        self.component_a.attack();
        self.component_b.attack();
    }
    fn release(&mut self) -> u32 {
        let a_release = self.component_a.release();
        let b_release = self.component_b.release();
        if a_release >= b_release {
            a_release
        } else {
            b_release
        }
    }
}
