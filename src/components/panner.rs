use crate::components::Component;
use crate::consts::*;

pub struct Panner<T: Component + Send> {
    component: T,
    cached_sample: Option<Sample>,
    lamp: f32,
    ramp: f32,
}

impl<T: Component + Send> Panner<T> {
    pub fn new(component: T, pan: f32) -> Panner<T> {
        assert!(component.channels() == 1);

        let (lamp, ramp) = Panner::<T>::channel_amps(pan);
        Panner {
            component,
            cached_sample: None,
            lamp,
            ramp,
        }
    }

    fn channel_amps(pan: f32) -> (f32, f32) {
        assert!(0.0 <= pan && pan <= 1.0); // 0.0 means all left; 1.0 means all right; 0.5 means middle.
        if pan <= 0.5 {
            (1.0, pan * 2.0)
        } else {
            (2.0 - 2.0 * pan, 1.0)
        }
    }
}

impl<T: Component + Send> Component for Panner<T> {
    fn get_sample(&mut self) -> Sample {
        match self.cached_sample {
            Some(sample) => {
                // right channel time
                self.cached_sample = None;
                sample * self.ramp
            }
            None => {
                // left channel time
                let sample = self.component.get_sample();
                self.cached_sample = Some(sample);
                sample * self.lamp
            }
        }
    }

    fn channels(&self) -> u16 {
        2
    }

    fn set_freq(&mut self, freq: f32) {
        self.component.set_freq(freq);
    }

    fn set_amp(&mut self, amp: f32) {
        self.component.set_amp(amp);
    }

    fn attack(&mut self) {
        self.component.attack()
    }
    fn release(&mut self) -> u32 {
        self.cached_sample = None;
        2 * self.component.release()
    }
}
