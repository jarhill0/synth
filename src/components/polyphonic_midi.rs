use crate::components::Component;
use crate::consts::*;
use std::sync::mpsc;

const NO_NOTE: u8 = 255;

pub struct PolyphonicMidiComponent<T: Component + Send> {
    components: Vec<T>,
    component_status: Vec<(u8, u32)>, // (MIDI note name, available on sample number)
    channels: u16,
    rx: mpsc::Receiver<MidiMessage>,
    t: u32,
}

impl<T: Component + Send> PolyphonicMidiComponent<T> {
    pub fn new(mut components: Vec<T>) -> (PolyphonicMidiComponent<T>, mpsc::Sender<MidiMessage>) {
        for component in components.iter_mut() {
            component.release();
            component.set_amp(0.0);
        }

        let channels =
            PolyphonicMidiComponent::assert_and_get_consistent_channel_count(&components);

        let voices = components.len();
        let component_status = vec![(NO_NOTE, 0); voices];

        let (tx, rx) = mpsc::channel();
        (
            PolyphonicMidiComponent {
                components,
                rx,
                component_status,
                channels,
                t: 0,
            },
            tx,
        )
    }

    fn assert_and_get_consistent_channel_count(components: &[T]) -> u16 {
        let mut num_channels = None;
        for component in components {
            match num_channels {
                Some(channel_count) => assert!(channel_count == component.channels()),
                None => {
                    num_channels = Some(component.channels());
                }
            }
        }
        num_channels.unwrap_or(1)
    }

    fn poll_channel(&mut self) {
        while let Ok(message) = self.rx.try_recv() {
            self.handle_message(message);
        }
    }

    fn handle_message(&mut self, message: MidiMessage) {
        match message {
            (MIDI_KEYDOWN, value, velocity) => {
                if velocity != 0 {
                    self.play_note(value, velocity);
                } else {
                    self.stop_play(value);
                }
            }
            (MIDI_KEYUP, value, _velocity) => {
                self.stop_play(value);
            }
            _ => (),
        }
    }

    fn note_index(&self, note: u8) -> Option<usize> {
        for (idx, pair) in self.component_status.iter().enumerate() {
            let playing_note = pair.0;
            if playing_note == note {
                return Some(idx);
            }
        }
        None
    }

    fn available_index(&self) -> usize {
        for (idx, pair) in self.component_status.iter().enumerate() {
            let (note_name, available_time) = pair;
            if *note_name == NO_NOTE && self.t >= *available_time {
                return idx;
            }
        }
        0 // oops, too many notes at once. I don't really care what we do.
    }

    fn assign_component(&mut self, idx: usize, freq: Option<f32>, amp: Option<f32>, attack: bool) {
        let component = self.components.get_mut(idx).unwrap();

        if let Some(freq) = freq {
            component.set_freq(freq);
        }

        if let Some(amp) = amp {
            component.set_amp(amp);
        }

        if attack {
            component.attack();
        }
    }

    fn play_note(&mut self, note: u8, velocity: u8) {
        match self.note_index(note) {
            Some(idx) => self.assign_component(idx, None, Some(velocity as f32 / 127.0), true),
            None => {
                let idx = self.available_index();
                self.assign_component(
                    idx,
                    Some(NOTE_FREQUENCIES[note as usize]),
                    Some(velocity as f32 / 127.0),
                    true,
                );
                self.component_status[idx].0 = note;
            }
        }
    }

    fn stop_play(&mut self, note: u8) {
        if let Some(idx) = self.note_index(note) {
            self.assign_component(idx, None, None, false);
            let component = self.components.get_mut(idx).unwrap();
            let available_time = self.t + component.release();
            self.component_status[idx] = (NO_NOTE, available_time);
        }
    }
}

impl<T: Component + Send> Component for PolyphonicMidiComponent<T> {
    fn get_sample(&mut self) -> Sample {
        self.poll_channel();
        self.t += 1;
        self.components
            .iter_mut()
            .map(|c| c.get_sample() * 0.6)
            .sum()
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn set_freq(&mut self, _freq: f32) {
        panic!("Nobody should be giving me frequency orders!");
    }

    fn set_amp(&mut self, _amp: f32) {
        panic!("Nobody should be giving me amplitude orders!");
    }

    fn attack(&mut self) {
        panic!("Nobody should be giving me attack orders!");
    }
    fn release(&mut self) -> u32 {
        panic!("Nobody should be giving me release orders!");
    }
}
