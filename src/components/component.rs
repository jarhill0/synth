use crate::consts::Sample;

pub trait Component {
    fn get_sample(&mut self) -> Sample;
    fn channels(&self) -> u16;

    fn set_freq(&mut self, freq: f32);
    fn set_amp(&mut self, amp: f32);
    fn attack(&mut self) {}
    fn release(&mut self) -> u32 {
        // returns # samples until release complete
        self.set_amp(0.0);
        0
    }
}
