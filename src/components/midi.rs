use crate::components::Component;
use crate::consts::*;
use std::sync::mpsc;

pub struct MidiComponent<T: Component + Send> {
    component: T,
    last_note: u8,
    rx: mpsc::Receiver<MidiMessage>,
}

impl<T: Component + Send> MidiComponent<T> {
    pub fn new(mut component: T) -> (MidiComponent<T>, mpsc::Sender<MidiMessage>) {
        component.set_amp(0.0);
        component.release();

        let (tx, rx) = mpsc::channel();
        (
            MidiComponent {
                component,
                rx,
                last_note: 0,
            },
            tx,
        )
    }

    fn poll_channel(&mut self) {
        while let Ok(message) = self.rx.try_recv() {
            self.handle_message(message);
        }
    }

    fn handle_message(&mut self, message: MidiMessage) {
        match message {
            (MIDI_KEYDOWN, value, velocity) => {
                if velocity != 0 {
                    self.play_note(value, velocity);
                } else {
                    self.stop_play(value);
                }
            }
            (MIDI_KEYUP, value, _velocity) => {
                self.stop_play(value);
            }
            _ => (),
        }
    }

    fn play_note(&mut self, note: u8, velocity: u8) {
        self.last_note = note;
        self.component.set_freq(NOTE_FREQUENCIES[note as usize]);
        self.component.set_amp(velocity as f32 / 127.0);
        self.component.attack();
    }

    fn stop_play(&mut self, note: u8) {
        if self.last_note == note {
            self.component.release();
        }
    }
}

impl<T: Component + Send> Component for MidiComponent<T> {
    fn get_sample(&mut self) -> Sample {
        self.poll_channel();
        self.component.get_sample()
    }

    fn channels(&self) -> u16 {
        self.component.channels()
    }

    fn set_freq(&mut self, _freq: f32) {
        panic!("Nobody should be giving me frequency orders!");
    }

    fn set_amp(&mut self, _amp: f32) {
        panic!("Nobody should be giving me amplitude orders!");
    }

    fn attack(&mut self) {
        panic!("Nobody should be giving me attack orders!");
    }
    fn release(&mut self) -> u32 {
        panic!("Nobody should be giving me release orders!");
    }
}
