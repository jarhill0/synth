use synth::components::{
    AdsrEnvelope, AmpModulator, PinkNoiseOscillator, PolyphonicMidiComponent, SawtoothOscillator,
    SineOscillator, TriangleOscillator, WaveAdder,
};
use synth::play;

fn main() {
    let mut voices = Vec::with_capacity(40);
    for _ in 0..40 {
        voices.push(AmpModulator::new(
            WaveAdder::new(
                SineOscillator::new(440.0, 1.0),
                SawtoothOscillator::new(440.0, 0.4),
            ),
            AdsrEnvelope::new(0.01, 0.2, 0.6, 0.3),
            1.0,
        ));
    }

    let (midi_component, midi_tx) = PolyphonicMidiComponent::new(voices);

    let tri_amp = AmpModulator::new(midi_component, TriangleOscillator::new(6.0, 1.0), 0.2);
    let noise_amp = AmpModulator::new(tri_amp, PinkNoiseOscillator::new(1.0), 0.3);

    play(noise_amp, midi_tx);
}
