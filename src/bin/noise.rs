use synth::components::{AdsrEnvelope, AmpModulator, MidiComponent, PinkNoiseOscillator};
use synth::play;

fn main() {
    let component = AmpModulator::new(
        PinkNoiseOscillator::new(0.3),
        AdsrEnvelope::new(0.05, 0.1, 0.0, 0.1),
        1.0,
    );

    let (midi_component, midi_tx) = MidiComponent::new(component);

    play(midi_component, midi_tx);
}
