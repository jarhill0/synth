use synth::components::{AdsrEnvelope, AmpModulator, PolyphonicMidiComponent, SineOscillator};
use synth::play;

fn main() {
    let mut voices = Vec::with_capacity(40);
    for _ in 0..40 {
        voices.push(AmpModulator::new(
            SineOscillator::new(440.0, 1.0),
            AdsrEnvelope::new(0.15, 1.2, 0.6, 0.1),
            1.0,
        ));
    }

    let (midi_component, midi_tx) = PolyphonicMidiComponent::new(voices);

    play(midi_component, midi_tx);
}
