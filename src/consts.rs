use note_frequencies::note_frequencies_32;

pub const SAMPLES_PER_SECOND: u32 = 44100;
pub type Sample = f32;
pub type MidiMessage = (u8, u8, u8);

pub use std::f32::consts::PI;

note_frequencies_32!(440.0);

pub const MIDI_KEYDOWN: u8 = 0x90;
pub const MIDI_KEYUP: u8 = 0x80;
